<!DOCTYPE html>
<html>
<head>
	<title>Městský Web</title>
	<link rel="stylesheet" type="text/css" href="volno.css">
</head>
<body>
	<header>
        <img id="logomesta" src="logo.png" alt="logo" height="200" width="250" >
    	<nav>
        	<?php include('volno_menu.php'); ?>
        </nav>
        <p><a href="index.php">Titulní stránka</a></p>
    </header>
    <div class="obsah">
    		<h1>Volnočasové aktivity</h1>
            <pre id="start">
            Město karlov je město s nádhernou přírodou můžete si zde odpočinout v krásných lesích, nebo 
            při procházce kolem jezera. Město Karlov nejen svým obyvatelům nabízí možnost zasportovat si 
            na několika sportovištích po celém městě. V příšzím roce započne výstavba nového fotbalového hřiště pro náš fotbalový tým.
            Staré fotbalové hřiště bude nadále udžované ale bude přístupnější ve větším časovém rozmezí pro veřejnost.

            Pro naše občany je zde plno dětských hřišť, na kterých sa vaše ratolesti jistě zabaví. Tyto dětské hřiště nádherně splývají
            s okolní přírodou.

            Dále zde máme další druhy aktivit, které je možno v našem malém městě vykonávat, jako je bruslení na zimním stadionu, 
            dále zde máme nové cyklostezky a atletický stadion.
                </pre>
    	<article>
            <h2>Fotogalerie:</h2>
            <nav id="fotky">
                <img src="rybnik.jpg" alt="rybnik" height="200" width="250">
                <img src="zimak.jpg" alt="zimak" height="200" width="250">
                <img src="atletika.jpg" alt="atletika" height="200" width="250">
            </nav>
            <h2>Články:</h2>
    		<nav id="prvni">
    			<h3>Připravujeme</h3>
    			<pre>
    		Vzhledem k trvající pandemii coronaviru, momentálně nelze připravovat
            žádné hromadné akce. Doufáme že se opět setkáme při nějaké akci jakmile to bude možné.
            Děkujeme za pochopení.
    			</pre>
    		</nav>
    	</article>
    	<footer>
    		<?php include('footer.php'); ?>  
    	</footer>
    </div>
</body>
</html>