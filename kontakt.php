<!DOCTYPE html>
<html>
<head>
	<title>Městský Web</title>
	<link rel="stylesheet" type="text/css" href="kontakt.css">
</head>
<body>
	<header>
        <img id="logomesta" src="logo.png" alt="logo" height="200" width="250" >
    	<nav>
        	<?php include('menu_kontakt.php'); ?>
        </nav>
        <p><a href="index.php">Titulní stránka</a></p>
    </header>
    <div class="obsah">
    		<h1>Kontakty</h1>
            <h3>Městský úřad</h3>
            <pre id="start">
                    Adresa: Město Karlov
                            Tyršova
                            591 01 Karlov

                    ID datové schránky: 5bgy01f
                    email: mukarlov@mukarlov.cz
                    telefon: 252 482 801

                    IČO: 001 10 210
                    DIČ: CZ001105210

                    Bankovní spojení: 
                        číslo účtu: 3584222, kód banky 0100             
                </pre>
                <a href="https://www.google.com/maps/place/591+01+Karlov/@49.6495454,15.8942428,14z/data=!4m5!3m4!1s0x470da16ecab58247:0x400af0f66153190!8m2!3d49.6470052!4d15.9144385?hl=cs-CZ"><img id="mapa" src="mapa.png" alt="mapa" height="300" width="350"></a>
    	<footer>
    		<?php include('footer.php'); ?> 
    	</footer>
    </div>
</body>
</html>