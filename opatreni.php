<!DOCTYPE html>
<html>
<head>
	<title>Městský Web</title>
	<link rel="stylesheet" type="text/css" href="opatreni.css">
</head>
<body>
	<header>
        <img id="logomesta" src="logo.png" alt="logo" height="200" width="250" >
    	<nav>
        	<?php include('menu_opatreni.php'); ?>
        </nav>
        <p><a href="index.php">Titulní stránka</a></p>
    </header>
    <div class="obsah">
    		<h1>Opatření</h1>
    	<article>
            <nav id="odkaz">
                <h3 class="jedna"><a href="https://onemocneni-aktualne.mzcr.cz/covid-19">Přehled situace v ČR: COVID 19</a></h3>
                <h3 class="dva"><a href="https://www.vlada.cz/scripts/detail.php?pgid=56">Informace vlády</a></h3>
                <h3 class="tri"><a href="https://koronavirus.mzcr.cz/">MZČR stránky ke koronaviru</a></h3>
                <h3 class="ctyri"><a href="https://www.mzcr.cz/">Ministerstvo zdravotnictví</a></h3>
            </nav>
            <h2>Články:</h2>
    		<nav id="prvni">
    			<h3>Opatřní k zápisům do ZŠ</h3>
    			<pre>
    		Ministerstvo školství, mládeže a tělovýchovy vydává v souvislosti s mimořádnými opatřeními 
                vlády k ochraně obyvatelstva v souvislosti s Koronavirem a onemocněním COVID-19 opatření k organizaci
                zápisů k povinné školní docházce pro školní rok 2020/2021.

    																		<a href="https://www.ksus.cz/">Více</a>
    			</pre>
    		</nav>
    		<nav id="druhy">
    			<h3>Uzavření škol</h3>
    			<pre>
    		S účinností ode dne 14. října 2020 je zakázána osobní přítomnost žáků v základních, středních,
                 základních uměleckých a dalších školách s výjimkou mateřských škol.


    																		<a href="https://koronavirus.mzcr.cz/ockovani-proti-covid-19/">Více</a>
    			</pre>
    		</nav>
    		<nav id="treti">
    			<h3>Úhrada objednaných nákupů pouze kartou</h3>
    			<pre>
    		Objednané dovážky potravin a léků bude řidič nechávat u dveří, abychom zabránili přímému
                kontaktu mezi obyvateli. Platba za dodané nákupy nebude probíhat na místě. 
                Chráníme tím vás i nás.

    																		<a href="https://ipk.nkp.cz/koronavirus-1/koronavirus">Více</a>
    			</pre>
    		</nav>
    		</nav>
    		<nav id="treti">
    			<h3>Uzavření občanského centra</h3>
    			<pre>
    		V souladu s nařízením vlády jsou do odvolání uzavřeny mimo jiné i veřejné knihovny. 
                S ohledem na skutečnost, že Městská knihovna Karlov je umístěna společně s informační kanceláří
                v Občanském centru Karlov, přistoupilo město k uzavření celého centra.

    																		<a href="https://onemocneni-aktualne.mzcr.cz/pes">Více</a>
    			</pre>
    		</nav>
    	</article>
    	<footer>
    		<?php include('footer.php'); ?>  
    	</footer>
    </div>
</body>
</html>