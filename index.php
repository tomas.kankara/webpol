<!DOCTYPE html>
<html>
<head>
	<title>Městský Web</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<header>
		<img id="logomesta" src="logo.png" alt="logo" height="200" width="250" >
    	<nav>
        	<?php include('uvod.php'); ?>
        </nav>
       	<img id="namesti" src="mesto.jpg" alt="mesto" height="350" width="600">
       	<h1>Staňte se dobrovolníkem města Karlov</h1>
        <pre>
        	Naše město uvítá jakoukoliv dobrovolnou činost. Ať už chcete pomoct v čemkoliv
        	nebojte se přihlásit na městském úřadě. Hledáme dobrovolníky pri různé druhy prací
        	ať už je to výsadba lesa, uklízení náměstí nebo čištění řeky.
        	Budeme se na vás těšit
        </pre>
    </header>
    <div class="obsah">
    		<h1>Aktuality města</h1>
    	<article>
    		<nav id="prvni">
    			<h3>Zimní údržba komunikací</h3>
    			<pre>
    			Údržba místních komunikací probíhá podle Plánu zimní údržby. Plán zimní údržby je dokument schválený Radou
    			města Karlov, podle kterého se provádí zimní údržba určených místních komunikací, chodníků a veřejných 
    			ploch.

    																		<a href="https://www.ksus.cz/">Více</a>
    			</pre>
    		</nav>
    		<nav id="druhy">
    			<h3>Očkování seniorů ve věku 80+</h3>
    			<pre>
    			15. ledna 2021 bude spuštěn centrální registrační systém na očkování proti onemocnění COVID-19 pro 
    			seniory nad 80 let. Město Karlov nabízí seniorům pomoc při registraci.

    																		<a href="https://koronavirus.mzcr.cz/ockovani-proti-covid-19/">Více</a>
    			</pre>
    		</nav>
    		<nav id="treti">
    			<h3>Výdej knih opět přes okénko</h3>
    			<pre>
    			Knihy je možné vrátit a zapůjčit pouze v předem domluveném termínu, dle instrukcí 
    			pracovníka Občanského centra Karlov.

    																		<a href="https://ipk.nkp.cz/koronavirus-1/koronavirus">Více</a>
    			</pre>
    		</nav>
    		</nav>
    		<nav id="treti">
    			<h3>Od 27.12.2020 platí 5.stupeň indexu PES</h3>
    			<pre>
    			Od 27.12. 2012 přecházíme do stupně 5. indexu PES. Ten by měl trvat 
    			minimálně do 10. ledna 2021. Knihovna je zavřena.

    																		<a href="https://onemocneni-aktualne.mzcr.cz/pes">Více</a>
    			</pre>
    		</nav>
    	</article>
    	<footer>
    		<?php include('footer.php'); ?> 
    	</footer>
    </div>
</body>
</html>