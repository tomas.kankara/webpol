<!DOCTYPE html>
<html>
<head>
	<title>Městský Web</title>
	<link rel="stylesheet" type="text/css" href="historie.css">
</head>
<body>
	<header>
        <img id="logomesta" src="logo.png" alt="logo" height="200" width="250" >
    	<nav>
        	<?php include('historie_menu.php'); ?>
        </nav>
        <p><a href="index.php">Titulní stránka</a></p>
    </header>
    <div class="obsah">
    		<h1>Historie města</h1>
            <pre id="start">
            Předchůdcem města, ležícího jižně od řeky Sázavy na významné obchodní cestě z Prahy do jižních Čech, 
            byl opevněný dvorec na žulovém ostrohu dnes zvaném Karlov, který tu zřejmě ve 2. polovině 11. stol. založil
            a po sobě pojmenoval jistý Benedikt (Beneš). Toto místo pak bylo hlavním sídlem pánů z Benešova až
            do r. 1318, kdy získali nedaleký hrad Konopiště a dvorec opustili. Při něm vznikla záhy osada, v r. 1327 poprvé 
            připomínaná jako městečko a v r. 1512 jako město, jejímž jádrem bylo prostorné tržiště (dnešní Masarykovo náměstí).
            V r. 1420 Město Benešov vyplenili husité, kteří vypálili zdejší minoritský klášter. V r. 1627
            proběhlo na Benešovsku vzbouření poddaných, protestujících proti násilné rekatolizaci.
            Obdobná událost se váže ke kostelu svatého Jakuba a Filipa na Chvojně, kde jsou pochováni zastřelení účastníci
            povstání nevolníků z roku 1775. Naše město bylo vtaženo i do třicetileté války, když  zde roku 1648 bojovali Švédové.
            V pohusitské době náležel Benešov mezi nejvýznamnější města v Čechách. Za vlády Jiřího z Poděbrad se v prostorách 
            dnes již zaniklého minoritského kostela konaly zemské sněmy. Zmíněný panovník zde v r. 1451 jednal o smíření
            s katolickou Evropou s papežským legátem Eneášem Silviem Piccolominim, pozdějším papežem Piem II. V r. 1473 vystoupila 
            na zdejším sněmu královna - vdova Johanka z Rožmitálu, aby se pokusila sjednat pro Čechy dočasný smír.
            V 18. stol. za Přehořovských a především za pánů z Vrtby se Benešov stal centrem kultury a vzdělanosti, 
            k čemuž přispívala hlavně nově založená piaristická kolej.
            K dalšímu významnému rozvoji Benešova napomáhalo již od 1. poloviny 18. století poštovní spojení s Prahou
            a zejména pak dokončení železniční trati z Prahy přes Benešov do Českých Budějovic a lokální trať do 
            Vlašimi. Od konce 19. stol. se stal majitelem zdejšího panství následník trůnu František Ferdinand d´Este,
            který je neodmyslitelně spjat se zámkem Konopiště. Za nacistické okupace v r. 1942 byla část města násilně 
            vystěhována kvůli zřízení rozsáhlého cvičiště SS.

            Převzato z <a href="https://benesov-city.cz/historie-mesta-benesov/ds-1132">webových stránek města Benešov </a>
                </pre>
    	<footer>
    		<?php include('footer.php'); ?> 
    	</footer>
    </div>
</body>
</html>